# Imports
from typing import Any, Dict, Optional, List
from fastapi import FastAPI, Depends
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from pydantic import BaseModel
from sqlalchemy.orm import query

# Init Stuff
app = FastAPI()
Base = declarative_base()
SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Dependency


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# Db Stuff


class AuditMixin(Base):
    __abstract__ = True
    id = Column(Integer, primary_key=True)


class Exams(AuditMixin):
    __tablename__ = "exam"
    name = Column(String(100))
    category_available = Column(Boolean, nullable=False)


class Categorys(AuditMixin):
    __tablename__ = "category"
    name = Column(String(100))
    exam_id = Column(Integer, ForeignKey("exam.id"), nullable=False)


class Subjects(AuditMixin):
    __tablename__ = "subject"
    name = Column(String(100))
    exam_id = Column(Integer, ForeignKey("exam.id"), nullable=False)
    category_id = Column(Integer, ForeignKey("category.id"), nullable=True)


class Topics(AuditMixin):
    __tablename__ = "topics"
    name = Column(String(100))
    topic_id = Column(Integer, ForeignKey("topics.id"), nullable=True)
    subject_id = Column(Integer, ForeignKey("subject.id"), nullable=True)


# Pydantic Models


class ExamModel(BaseModel):
    name: str
    category_available: bool


class ExamAdd(BaseModel):
    exam_id: int
    data: Dict


# Endpoints


@app.get("/")
async def root():
    return {"result": "success", "message": "Api is up!"}


@app.post("/create_exam")
async def create_exam(exam: ExamModel, db: Session = Depends(get_db)):
    exam_obj = Exams(name=exam.name, category_available=exam.category_available)
    db.add(exam_obj)
    db.commit()
    return {"result": "success", "message": "Exam created!"}


@app.get("/list_exam")
async def list_exam(db: Session = Depends(get_db)):
    data = [item for item in db.query(Exams).all()]
    return {"result": "success", "message": "Exam available!", "data": data}


@app.post("/add_item")
async def add_item(examadd: ExamAdd, db: Session = Depends(get_db)):
    exam = db.query(Exams).filter(Exams.id == examadd.exam_id).one_or_none()
    if not exam:
        return {"result": "error", "message": "Invalid exam id!"}
    if not exam.category_available:
        if examadd.data.get("category"):
            return {"result": "error", "message": "Exam does not support category!"}
        for item in examadd.data.get("subjects"):
            sub = Subjects(name=item.get("name"), exam_id=exam.id)
            db.add(sub)
            db.flush()
            rec_add_topic(db=db, list_top=item.get("topics"), subject_id=sub.id)
    else:
        cat = Categorys(name=examadd.data.get("category"), exam_id=exam.id)
        db.add(cat)
        db.flush()
        for item in examadd.data.get("subjects"):
            sub = Subjects(name=item.get("name"), exam_id=exam.id, category_id=cat.id)
            db.add(sub)
            db.flush()
            rec_add_topic(db=db, list_top=item.get("topics"), subject_id=sub.id)
    db.commit()
    return {"result": "success", "message": "Attribute added successfully!"}


@app.get("/get_exam")
async def get_exam(exam_id: int, db: Session = Depends(get_db)):
    resp = get_exam_data(db=db, exam_id=exam_id)
    return resp


# Utils


def rec_add_topic(
    db: Session,
    list_top: List,
    subject_id: Optional[int] = None,
    topic_id: Optional[int] = None,
) -> Dict[str, str]:
    for topics in list_top:
        top = Topics(name=topics.get("name"), subject_id=subject_id, topic_id=topic_id)
        db.add(top)
        db.flush()
        if topics.get("sub_topics"):
            rec_add_topic(db=db, list_top=topics.get("sub_topics"), topic_id=top.id)
    return {"result": "success", "message": "Topic added!"}


def get_exam_data(db: Session, exam_id: int) -> Dict[Any, Any]:
    data = {}
    exam = db.query(Exams).filter(Exams.id == exam_id).one_or_none()
    data.update({"exam": exam.name})
    if not exam:
        return {"result": "error", "message": "Invalid exam id!"}
    if exam.category_available:
        cat = db.query(Categorys).filter(Categorys.exam_id == exam.id).all()
        list_cat = []
        for item in cat:
            subj = db.query(Subjects).filter(Subjects.category_id == item.id).all()
            list_subj = []
            for sitem in subj:
                list_subj.append(
                    {
                        "name": sitem.name,
                        "topics": rec_get_topics(db=db, subj_id=sitem.id),
                    }
                )
            list_cat.append({"name": item.name, "subjects": list_subj})
        return {
            "result": "success",
            "message": "Exam data!",
            "exam": exam.name,
            "category": list_cat,
        }
    subj = db.query(Subjects).filter(Subjects.category_id == exam.id).all()
    list_subj = []
    for sitem in subj:
        list_subj.append(
            {"name": sitem.name, "topics": rec_get_topics(db=db, subj_id=sitem.id)}
        )
    return {
        "result": "success",
        "message": "Exam data!",
        "exam": exam.name,
        "subject": list_subj,
    }


def rec_get_topics(
    db: Session, topic_id: Optional[int] = None, subj_id: Optional[int] = None
) -> List[Dict]:
    all_top = []
    topics = (
        db.query(Topics)
        .filter(Topics.topic_id == topic_id, Topics.subject_id == subj_id)
        .all()
    )
    for top in topics:
        all_top.append(
            {"name": top.name, "sub_topic": rec_get_topics(db=db, topic_id=top.id)}
        )
    return all_top
